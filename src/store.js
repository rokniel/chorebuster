import Vue from 'vue'
import Vuex from 'vuex'
import Axios from 'axios'

Vue.use(Vuex)

const chores = {
  namespaced: true,
  state: {
    myValue: 10001001,
    Chores: [{id:1, name: 'Eat dinner', points: 1}, {id:2, name: 'Clean room', points: 3}],
  },
  mutations: {
    RemoveChore(state, chore) {state.Chores = (state.Chores.filter(obj => obj.id !== chore.id))},
    AddChore(state, newChore) {const id = Math.floor((Math.random()*1000000)+1);newChore.id = id;state.Chores.push(newChore)},
  },
  actions: {
    CompleteChore({dispatch, commit}, chore) {commit('RemoveChore', chore); dispatch('buckets/AddPointsToBuckets', chore, {root:true})},
    AddNewChore({commit}, newChore) {commit('AddChore', newChore)},
  },
  getters: {
    GetChores(state) {return state.Chores},
  },
}

const buckets = {
  namespaced: true,
  state: {
    Buckets: [], 
  },
  mutations: {
    AddPointsToAllBuckets(state, chore) {state.Buckets.forEach(obj => obj.currentPoints += chore.points)},
    AddBucket(state, newBucket) {const id = Math.floor((Math.random()*1000000)+1);newBucket.id = id; newBucket.currentPoints=0;state.Buckets.push(newBucket)},
    SetInitialBuckets(state, receivedBuckets) {state.Buckets = receivedBuckets},
  },
  actions: {
    AddPointsToBuckets({commit}, chore) {commit('AddPointsToAllBuckets', chore)},
    AddNewBucket({commit}, newBucket) {commit('AddBucket', newBucket)},
    InitializeBuckets({commit}) {Axios.get('http://localhost:3000/api').then((receivedBuckets) => {console.log(receivedBuckets.data[0]);commit('SetInitialBuckets', receivedBuckets.data)}).catch((err) => {console.log(err)})},
  },
  getters: {
    GetBuckets(state) {return state.Buckets.filter(obj => obj.full !== true)},
  },
}

export default new Vuex.Store({
  modules: {
    chores: chores,
    buckets: buckets,
  }
})

